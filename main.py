import requests as r
from twilio.rest import Client
import config as c
# ---- Openweather:
par = {
    "lon": 19.6261,
    "lat": 52.244,
    "appid": c.API_KEY,
    "exclude": "current,minutely,daily"
}

response = r.get(url="https://api.openweathermap.org/data/2.5/forecast", params=par)
response.raise_for_status()
# ----

# ---- Twilio:
account_sid = 'AC842c02c1fb8f047337dc9388f30e9a0c'
auth_token = c.AUTH_TOKEN
# ----

weather_data = response.json()
weather_slice = weather_data['list'][:12]

# # 1:
# umb = [x['weather'][0]['id'] for x in weather_data['list'][:12] if x['weather'][0]['id'] < 700]
# print(umb)
#
# # 2:
# if [ True for wind in weather_data['list'][:12] if wind['weather'][0]['id'] < 700]:
#     print('Umbrella plz.')
#
# 3:

will_rain = False
for h_data in weather_slice:
    condition_code = int(h_data['weather'][0]['id'])
    if condition_code < 700:
        will_rain = True
        break
# weather_data['list'][:12]['weather'][0]['id']
if will_rain:
    client = Client(account_sid, auth_token)
    message = client.messages.create(
        from_=c.T_NUMBER,
        body='Oops... It\'s going to rain today!. Remember to bring an ☂️',
        to=c.MY_NUMBER
    )

print(message.status)
